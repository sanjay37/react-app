import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          <a href="" className="paragraph">Walmart Shoes Smart Container.</a>
        </p>
      </header>
    </div>
  );
}

export default App;
